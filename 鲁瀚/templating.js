let nunjucks = require('nunjucks');

function creatConf(path, opts) {
    path = path || 'views';
    opts = opts || {}
    let env = nunjucks.configure('views', opts)
    return env;
}

module.exports = async (etc, next) => {
    let env = creatConf();
    etc.render = function (path, opts) {
        etc.body = env.render(path, opts)
    }
    await next();
}