'use strict'

const Router = require('koa-router');
const Koa = require('koa');
const  bodyparser = require('koa-bodyparser');
const  static_fn = require('koa-static');
const templating = require('./templating');
const bodyParser = require('koa-bodyparser');
const  controllers = require('./controllers');
const  app = new Koa();

app.use(bodyparser());

app.use(static_fn(__dirname + '/static'))

app.use(templating)



app.use(controllers())




app.listen(3000,()=>{
    console.log("服务器运行的地址是http://127.0.0.1:3000")
})